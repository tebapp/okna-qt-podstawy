import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2

Window {
    id: glowne
    visible: true
    width: 640
    height: 480
    title: qsTr("Pierwsze okno w Qt!")
    color: Qt.rgba(1,0.76,0.3,1)

    Rectangle {
        id: prostokat
        anchors.fill: parent
        color: "#fff"
    }

    Button {
         id: przycisk1

        onClicked: console.log(down)//Qt.quit()
        x: 300
        y: 300
        width: 200
        height: 40

        contentItem: Text {
            id: btnText
            text: "Potwierdź"
            color: parent.down ? "#ff0" : "#fff"
            font.pixelSize: 20
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        background: Rectangle {
            color: "#7812AA"
            opacity: parent.down ? 0.5 : 1
            //color: parent.down ? "#fff" : "#7812AA"
        }
    }

    EditPool {
        id: edit1
        caption: "Edycja tekstu"

        Component.onCompleted:  {
            changeText.connect(zmienTekst)
        }
    }


    EditPool {
        id:edit2
        anchors.top: edit1.bottom
        caption: "Edycja nazwy okna"
        Component.onCompleted:  {
            changeText.connect(zmienNazweOkna)
        }
    }

    EditPool {
        id: edit3
        anchors.top: edit2.bottom
        caption: "Edycja przycisku"
        Component.onCompleted:  {
            changeText.connect(zmienTekstPrzycisk)
        }
    }

    function zmienTekst(tekst) {
            pierwszy_tekst.text = tekst
    }

    function zmienNazweOkna(tekst) {
        glowne.title = tekst
    }

    function zmienTekstPrzycisk(tekst) {
        btnText.text = tekst
    }



    Text {
        id: pierwszy_tekst
        text: qsTr("Tutaj jest nowy tekst!")
        font.pixelSize: 40
        font.italic: true
        //anchors.centerIn: prostokat
        anchors.right: prostokat.right
        anchors.bottom: prostokat.bottom
        anchors.bottomMargin: prostokat.height/2
        color: "#f00"
        MouseArea {
            id: mouse1
            anchors.fill: parent
            hoverEnabled: true
            acceptedButtons: Qt.LeftButton | Qt.RightButton | Qt.MiddleButton
            onClicked: {
                if (mouse.button === Qt.RightButton)
                pierwszy_tekst.text = "Nowe możliwości, nowe wartości"
            }
            onEntered: pierwszy_tekst.color = "#fff"
            onExited: pierwszy_tekst.color ="#f00"
        }
    }
}
