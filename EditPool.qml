import QtQuick 2.0

Rectangle {
    property alias caption: editCaption.text

    signal changeText(string tekst)

    width: 200
    height: 70
    Text {
        id: editCaption
        text: qsTr("text")
        font.bold: true
        anchors.top: parent.top
    }

    Rectangle {
        anchors.top: editCaption.bottom
        width: 200
        height: 50
        color: "#fff"
        clip: true
        border.color: "#0f0"
        radius:  10
        TextEdit {
            anchors.leftMargin: 5
            anchors.fill: parent
            //text: ""
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: parent.height-20
            color: "#00f"
            onTextChanged: changeText(text) //pierwszy_tekst.text = text
        }

    }

}
